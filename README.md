# Docker Setup for NYSD Archives

This is a 3 docker setup that holds docker setup that can be duplicated on an computer/server that has Docker & Docker Compose installed.  

## Digital Ocean Setup
The dockers run off the mounted 60GB volume `/mnt/volume_sfo2_01/deploy`.  
There you will find this git repo along with the following directories `html` `data`.  
The `html` directory is mounted into both the `web` container and the `app` containers and mapped to the following directory `/var/www/html`. This directory holds the site files.
The `data` directory holds the database directory and is mapped to `/var/lib/mysql`.  

## Database Access
If you want to access the database using PHPMyAdmin then you can just uncomment the following lines in `docker-compose.yaml`.
```yaml
#  phpmyadmin:
#    image: phpmyadmin/phpmyadmin
#    ports:
#      - "8080:80"
#    networks:
#      - data
#    environment:
#      - "PMA_HOST=data"
```  
The run the following command `docker-compose up -d`. Afterwards you can access PHPMyAdmin via [http://old.newyorksocialdiary.com:8080/](http://old.newyorksocialdiary.com:8080/).  
To take down PHPMyAdmin follow comment out the same section and then run `docker-compose up -d --remove-orphans`.  

## Troubleshooting
### Reboot
After a reboot the dockers should come up after a 2 min delay automatically via a cron.  
`@reboot sleep 120 cd /mnt/volume_sfo2_01/deploy && docker-compose up -d`  

If the dockers do not come or the site is not accessible log into the server and issue the following commands.
```bash
cd /mnt/volume_sfo2_01/deploy
docker-compose up -d
```
Afterwards confirm that the containers come back up using `docker ps`.  

**More instructions can be found on the server at `/mnt/volume_sfo2_01/deploy/instructions.txt`. You will need to be root user to access.**  

